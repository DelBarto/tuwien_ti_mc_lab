#include "main.h"


void SysTick_Handler(void) {
  counter++;
}

int genRand(void){
	randStart = (randStart*13 + counter) % 400;
	return (int) randStart/100;
}

int genRandOrient(void){
	orient_arr = genRand();
	return orient_arr;
}

void send_USART(int len, char* data){
	for(int i = 0; i < len; i++){
		while(USART_GetFlagStatus(USART2, USART_FLAG_TXE)!=SET){};
		USART_SendData(USART2, data[i]);
	}
}

void send_USART_int(int val){
	int temp = 0;
	for(int i = 3; i >= 0; i--){
		int digit =  (val-temp)/pow(10,i);
		temp += digit*pow(10,i);
		send_USART(1, (char[]) {'0'+digit});
	}
}

void printHelp(void){
	send_USART(22, "[1...5] ... Dificulty\n");
	send_USART(35, "[d|D]   ... show current dificulty\n");
	send_USART(35, "[p|P]   ... pause/unpause the game\n");
	send_USART(28, "[r|R]   ... reset highscore\n");
	send_USART(45, "[s|S]   ... show current score and highscore\n");
	send_USART(17, "else    ... help\n");
	if (start) send_USART(27, "Enter x to start the game!\n");
}

void incScore(int inc){
	score += inc;
	send_USART(15, "Current score: ");
	send_USART_int(score);
	send_USART(1, "\n");
		
}

void gameOver(void){
	if (score == 0) return;
	send_USART(32, "---------- GAME OVER ----------\n");
	if (highscore < score) {
		send_USART(15, "New highscore!\n");
		highscore = score;
	}
	send_USART(11, "Score:     ");
	send_USART_int(score);
	send_USART(1, "\n");
	send_USART(11, "Highscore: ");
	send_USART_int(highscore);
	send_USART(1, "\n");
	send_USART(32, "-------------------------------\n");
	score = 0;
}

int main(void)
{	
		//time_t t;
		//srand((unsigned) time(&t));
		volatile uint32_t counter = 0;
		SysTick_Config(SystemCoreClock/10000);
		initField(field);
    
    // Wie in Beispiel 2A besprochen, konfigurieren wir gleich zu Beginn auch die Aufteilung von Gruppen- und Subgruppenpriorit�ten des NVIC
    // Wir teilen die 4 Bit wieder 2-2 auf und k�nnen damit sowohl f�r Gruppe als auch Subgruppe jeweils die Level 0-3 einstellen
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	
		//initialization of interfaces
		USART_init();
		ADC_init();
		SPI_init();
		TIM3_init();
		
		// setup of the rest
		initDispl();
		initField(field);
		drawField(field);
		send_USART(21, "Initialization done!\n");
		printHelp();
		
		TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);
			
		
		
		while (true){
		}
}



// --------------------- Schritt 4: Interrupt Handler -----------------------------


// Die Funktionsnamen der Interrupt Handler finden wir wieder in der "startup_stm32f334x8.s" Datei
// In unserem Fall also "EXTI15_10_IRQHandler" und "USART2_IRQHandler"
// ERINNERUNG: Die Funktionen m�ssen GENAU DIESEN Namen haben (case-sensitiv)

// Fangen wir mit dem USART2 Interrupt an
void USART2_IRQHandler()
{
	int recvd;
	recvd=USART_ReceiveData(USART2);

	
	if ((recvd>='1')&&(recvd<='5')){
		speed = recvd-'0';
		speedcounter = 0;
		send_USART(16, "Dificulty is set to ");
		send_USART(2, (char[]) {recvd, '\n'});
		//gameOver(); // should changing speed set you to gameOver? (if so uncommnet)
		
		
	}	else if ((recvd=='d') || (recvd=='D')){
		send_USART(25, "The dificutlty is set to ");
		send_USART(2, (char[]) {'0'+speed, '\n'});
		
		
	}	else if ((recvd=='s') || (recvd=='S')){
		send_USART(15, "Current score: ");
		send_USART_int(score);
		send_USART(1, "\n");
		send_USART(15, "Highscore:     ");
		send_USART_int(highscore);
		send_USART(1, "\n");
	
		
	}	else if ((recvd=='r') || (recvd=='R')){
		highscore = 0;
		send_USART(22, "Highscore is reseted!\n");
		
		
	}	else if ((recvd=='p') || (recvd=='P')){
		pause = !pause;
		if (pause){
			TIM_ITConfig(TIM3, TIM_IT_Update, DISABLE);
			ADC_ITConfig(ADC1, TIM_IT_Update, DISABLE);
			send_USART(8, "Paused!\n");
		} else {
			TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);
			ADC_ITConfig(ADC1, TIM_IT_Update, ENABLE);
			send_USART(9, "Running!\n");
		}
		
	}	else if ((recvd=='x') || (recvd=='X')){
		if(start){
			start = false;
			speedcounter = 0;
			clearField(field);
			send_USART(26, "Game started - Good Luck!\n");
		}
		
	}	else {
		printHelp();
	}
	
}

void ADC1_IRQHandler(void)
{
	if (!pause && !start){
		if (ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == SET){
			adcValue_y = ADC_GetConversionValue(ADC1);
			//ADC_ClearFlag(ADC1, ADC_FLAG_EOC);
		}
		while (ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) != SET);
		if (ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == SET){
			adcValue_x = ADC_GetConversionValue(ADC1);
			//ADC_ClearFlag(ADC1, ADC_FLAG_EOC);
		}
		
		//send_USART_int(adcValue_x);
		//send_USART(1, " ");
		//send_USART_int(adcValue_y);
		//send_USART(1, "\n");
			
		
		// Channel sequnce pos 1 -> x axis
		if ((adcValue_x < 300) && !return_xjoystick && !return_yjoystick){
			//send_USART_int(adcValue_x);
			//send_USART(6, " left\n");
			if (correctAction(field, 1, orient_arr, 1) == 1) incScore(1);
			else gameOver();
			getRandArrow(field, genRandOrient());
			return_xjoystick = true;
			
		} else if ((adcValue_x > 3700) && !return_xjoystick && !return_yjoystick){
			//send_USART_int(adcValue_x);
			//send_USART(7, " right\n");
			if(correctAction(field, 1, orient_arr, 3) == 1)incScore(1);
			else gameOver();
			getRandArrow(field, genRandOrient());
			return_xjoystick = true;
			
		} else if ((adcValue_x > 1000) && (adcValue_x < 3000) && return_xjoystick) {
			return_xjoystick = false;
		}
		
		// Channel sequnce pos 2 -> y axis
		if ((adcValue_y < 300) && !return_yjoystick && !return_xjoystick){
			//send_USART_int(adcValue_y);
			//send_USART(6, " down\n");
			if (correctAction(field, 1, orient_arr, 2) == 1)incScore(1);
			else gameOver();
			getRandArrow(field, genRandOrient());
			return_yjoystick = true;
			
		} else if ((adcValue_y > 3700) && !return_yjoystick && !return_xjoystick){
			//send_USART_int(adcValue_y);
			//send_USART(4, " up\n");
			if (correctAction(field, 1, orient_arr, 0) == 1)incScore(1);
			else gameOver();
			getRandArrow(field, genRandOrient());
			return_yjoystick = true;
			
		} else if ((adcValue_y > 1000) && (adcValue_y < 3000) && return_yjoystick) {
			return_yjoystick = false;
		}
	}
	
	
	
	ADC_ClearITPendingBit(ADC1, ADC_IT_EOS);
}



void TIM2_IRQHandler(void)
{

  TIM_ClearITPendingBit(TIM2,TIM_IT_Update);
}



void TIM3_IRQHandler(void)
{
	//genRand();
	if (!start){
		if (speedcounter == (6-speed)){
			
				moveField(field, 1);
				drawField(field);
			
			
				if (isEmpty(field)){
					gameOver();
					getRandArrow(field, genRandOrient());
				}
				speedcounter = 0;
			
		} else {
			speedcounter += 1;
		}
	} else {
		if (speedcounter == 20){
			
				drawArrows(field, (int[]) {genRand(), genRand(),genRand(),genRand()});
				drawField(field);
				
				speedcounter = 0;
				
		} else {
			speedcounter += 1;
		}
	}
		
	TIM_ClearITPendingBit(TIM3,TIM_IT_Update);
}
// ----------------------------------------------------------------------------

