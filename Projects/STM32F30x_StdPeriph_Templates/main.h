/*
  ******************************************************************************
  MIKROCOMPUTER LABOR - HOME-STATION

  HIER BITTE NAMEN + MATRIKELNUMMER EINTRAGEN

  In diesem Projekt gilt:
  *=============================================================================
  *        SYSCLK(Hz)                             | 64000000
  *-----------------------------------------------------------------------------
  *        AHB Prescaler                          | 1
  *-----------------------------------------------------------------------------
  *        APB2 Prescaler                         | 1
  *-----------------------------------------------------------------------------
  *        APB1 Prescaler                         | 2
  *=============================================================================
  ******************************************************************************
*/

/* Includes ------------------------------------------------------------------*/
#include "stm32f30x_conf.h"
#include "./utils/init_funct.h"
#include "./utils/disp.h"

#include <math.h>
#include <stdlib.h>

#define MODS 4
#define WIDTH  8


typedef int bool;
enum { false, true };


int speed = 3; // 1...5 are the possible dificulties
int speedcounter = 0;

int score = 0;
int highscore = 0;

int adcValue_x = 2000;
int adcValue_y = 2000;
		 
int orient_arr = 0;

uint64_t field[WIDTH];
		

int randStart = 0;

uint32_t counter = 0;

bool pause = false;
bool start = false; // if true a startscreen is activated

bool return_xjoystick = false;
bool return_yjoystick = false;
		
