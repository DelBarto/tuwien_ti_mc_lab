#include "stm32f30x_conf.h"



#define SPI_INT SPI1
#define MODS 4
#define WIDTH  8

void send_SPIsingle(uint16_t data);

void send_SPI(uint16_t data);

void send_SPIall(uint16_t data);

void clearField(uint64_t *field);

void fillField(uint64_t *field);

void initField(uint64_t *field);

void initDispl(void);

void moveField(uint64_t *field, int left);

void drawField(uint64_t *field);

void getRandArrow(uint64_t *field, int direction);

int isEmpty(uint64_t *field);

int compareTiles(uint8_t *tile, uint8_t *compareTo);

int arrowInTile(uint64_t *field, int tile, int orientation);

int correctAction(uint64_t *field, int tile, int orient_arrow, int direction);

void drawArrows(uint64_t *field, int *orient_tile);


/*
uint64_t uarrow[] = {0x0000000000, 
											0x1000000000,
											0x3000000000,
											0x7E00000000,
											0x7E00000000,
											0x3000000000,
											0x1000000000,
											0x0000000000};

uint64_t larrow[] = {0x0000000000,
											0x1800000000,
											0x1800000000,
											0x1800000000,
											0x7E00000000,
											0x3C00000000,
											0x1800000000,
											0x0000000000};

uint64_t darrow[] = {0x0000000000,
											0x0800000000,
											0x0C00000000,
											0x7E00000000,
											0x7E00000000,
											0x0C00000000,
											0x0800000000,
											0x0000000000};


uint64_t rarrow[] = {0x0000000000,
											0x1800000000,
											0x3C00000000,
											0x7E00000000,
											0x1800000000,
											0x1800000000,
											0x1800000000,
											0x0000000000};
*/

