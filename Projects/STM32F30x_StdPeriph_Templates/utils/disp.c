#include "disp.h"

void send_SPIsingle(uint16_t data){	
	while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) != SET){};
	GPIOA->ODR &= !(1<<4);
	SPI_I2S_SendData16(SPI_INT, data);
	while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY)==SET){};
	GPIOA->ODR |=1<<4;
	
}

void send_SPI(uint16_t data){	
	while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY)==SET){};
	SPI_I2S_SendData16(SPI_INT, data);
}

void send_SPIall(uint16_t data){	
	while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) != SET){};
	GPIOA->ODR &= !(1<<4);
	for(int j = MODS-1; j >= 0; j--){
		send_SPI(data);
	}
	while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY)==SET){};
	GPIOA->ODR |=1<<4;
	
}


void clearField(uint64_t *field){
	for(int i = 0; i < WIDTH; i++){
		field[i] = 0;
	}
}

void fillField(uint64_t *field){
	for(int i = 0; i < WIDTH; i++){
		field[i] = 0xFFFFFFFF;
	}
}

void initField(uint64_t *field){
	clearField(field);
	
}


void initDispl(void){
	send_SPIall(0x0C01);	// deactivate shutdown mode
	send_SPIall(0x0900);	// deactivate decode mode
	send_SPIall(0x0B07);	// set scan limit to 7
	send_SPIall(0x0C01);	// activate normal mode
	send_SPIall(0x0A00); // lowest Brightness
}

void moveField(uint64_t *field, int down){
	for(int i = 0; i < WIDTH; i++){
		field[i] = field[i]>>down;
	}
}

void drawField(uint64_t *field){
		for(int i = 0; i < WIDTH; i++){
			while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_TXE) != SET){};
			GPIOA->ODR &= !(1<<4);
			for(int j = MODS-1; j >= 0; j--){
				send_SPI(0x0100*(i+1)+((field[i]>>j*2*4)&0x00FF));
			}
			while(SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY)==SET){};
			GPIOA->ODR |=1<<4;
		}
}


void getRandArrow(uint64_t *field, int direction){ 
	//0: up, 1: left, 2: down, 3: right
	if(direction == 0){
		field[0] = 0x0000000000;
		field[1] = 0x1000000000;
		field[2] = 0x3000000000;
		field[3] = 0x7E00000000;
		field[4] = 0x7E00000000;
		field[5] = 0x3000000000;
		field[6] = 0x1000000000;
		field[7] = 0x0000000000;
	} else if(direction == 1){
		field[0] = 0x0000000000;
		field[1] = 0x1800000000;
		field[2] = 0x1800000000;
		field[3] = 0x1800000000;
		field[4] = 0x7E00000000;
		field[5] = 0x3C00000000;
		field[6] = 0x1800000000;
		field[7] = 0x0000000000;
	}	else if(direction == 2){
		field[0] = 0x0000000000;
		field[1] = 0x0800000000;
		field[2] = 0x0C00000000;
		field[3] = 0x7E00000000;
		field[4] = 0x7E00000000;
		field[5] = 0x0C00000000;
		field[6] = 0x0800000000;
		field[7] = 0x0000000000;
	}	else if(direction == 3){
		field[0] = 0x0000000000;
		field[1] = 0x1800000000;
		field[2] = 0x3C00000000;
		field[3] = 0x7E00000000;
		field[4] = 0x1800000000;
		field[5] = 0x1800000000;
		field[6] = 0x1800000000;
		field[7] = 0x0000000000;
	} else {
		field[0] = 0x0000000000;
		field[1] = 0x6200000000;
		field[2] = 0x6400000000;
		field[3] = 0x0800000000;
		field[4] = 0x0800000000;
		field[5] = 0x6400000000;
		field[6] = 0x6200000000;
		field[7] = 0x0000000000;
	}
}


//check if full arrow is in tile (bottom 0 ... up 3)
int arrowInTile(uint64_t *field, int tile, int orientation){
	
	uint8_t lineCont;
	lineCont = field[2]>>((tile)*8)&0xFF;
	
	if(orientation == 0){
		if (lineCont == 0x30){
			return 1;
		} else if (lineCont == 0x30<<1){
			return 1;
		} else if (lineCont == 0x30>>1){
			return 1;
		}			
	} else if (orientation == 1){
		if (lineCont == 0x18){
			return 1;
		} else if (lineCont == 0x18<<1){
			return 1;
		} else if (lineCont == 0x18>>1){
			return 1;
		}					
	} else if (orientation == 2){
		if (lineCont == 0x0C){
			return 1;
		} else if (lineCont == 0x0C<<1){
			return 1;
		} else if (lineCont == 0x0C>>1){
			return 1;
		}		
	} else if (orientation == 3){
		if (lineCont == 0x3C){
			return 1;
		} else if (lineCont == 0x3C<<1){
			return 1;
		} else if (lineCont == 0x3C>>1){
			return 1;
		}		
	}
	return 0;
}

int compareTiles(uint8_t *tile, uint8_t *compareTo){
	for(int i = 0; i < WIDTH; i++){
		if (tile[i] != compareTo[i]) return 0;
	}
	return 1;
}

int correctAction(uint64_t *field, int tile, int orient_arrow, int direction){
	if (orient_arrow == direction){
		if (arrowInTile(field, tile, orient_arrow) == 1){
			return 1;
		}
	}
	return 0;
}

void drawArrows(uint64_t *field, int *orient_tile){
	clearField(field);
	int shiftScale = 8;
	for(int i = 0; i < MODS; i++){
		if(orient_tile[i] == 0){
		field[0] |= 0x00<<i*shiftScale;
		field[1] |= 0x10<<i*shiftScale;
		field[2] |= 0x30<<i*shiftScale;
		field[3] |= 0x7E<<i*shiftScale;
		field[4] |= 0x7E<<i*shiftScale;
		field[5] |= 0x30<<i*shiftScale;
		field[6] |= 0x10<<i*shiftScale;
		field[7] |= 0x00<<i*shiftScale;
	} else if(orient_tile[i] == 1){
		field[0] |= 0x00<<i*shiftScale;
		field[1] |= 0x18<<i*shiftScale;
		field[2] |= 0x18<<i*shiftScale;
		field[3] |= 0x18<<i*shiftScale;
		field[4] |= 0x7E<<i*shiftScale;
		field[5] |= 0x3C<<i*shiftScale;
		field[6] |= 0x18<<i*shiftScale;
		field[7] |= 0x00<<i*shiftScale;
	}	else if(orient_tile[i] == 2){
		field[0] |= 0x00<<i*shiftScale;
		field[1] |= 0x08<<i*shiftScale;
		field[2] |= 0x0C<<i*shiftScale;
		field[3] |= 0x7E<<i*shiftScale;
		field[4] |= 0x7E<<i*shiftScale;
		field[5] |= 0x0C<<i*shiftScale;
		field[6] |= 0x08<<i*shiftScale;
		field[7] |= 0x00<<i*shiftScale;
	}	else if(orient_tile[i] == 3){
		field[0] |= 0x00<<i*shiftScale;
		field[1] |= 0x18<<i*shiftScale;
		field[2] |= 0x3C<<i*shiftScale;
		field[3] |= 0x7E<<i*shiftScale;
		field[4] |= 0x18<<i*shiftScale;
		field[5] |= 0x18<<i*shiftScale;
		field[6] |= 0x18<<i*shiftScale;
		field[7] |= 0x00<<i*shiftScale;
	}
	}
}

int isEmpty(uint64_t *field){
	for(int i = 0; i < WIDTH; i++){
		if(field[i] != 0){
			return 0;
		}
	}
	return 1;
}

