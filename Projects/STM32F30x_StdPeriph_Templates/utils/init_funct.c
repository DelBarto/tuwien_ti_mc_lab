#include "init_funct.h"

// INITIALIZATION ROUTINE FOR THE USART INTERFACE
void USART_init(void)
{
	USART_InitTypeDef  USART_InitStruct;
	GPIO_InitTypeDef   GPIO_InitStruct;
	NVIC_InitTypeDef   NVIC_InitStruct;
		
	// Im einfachen asynchronen Modus ohne Hardware Flow Control ben�tigen wir f�r die USART zwei Leitungen: RX und TX (siehe VO)
	// Der STM32F334R8 besitzt drei USART Einheiten (USART1-3), die jeweils mit mehreren Pins verbunden werden k�nnen
	// Welche Konfiguration verwenden wir nun? Hier hilft wieder das User Manual des Nucleo Boards (PDF, Kapitel 6.8, S. 25)
	// Der USART Kanal, der �ber den ST-Link mit dem PC verbunden werden kann, ist von USART2 und hat folgende Pinkonfiguration:
	// Pin A.2 USART2 TX
	// Pin A.3 USART2 RX
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);  
	
	// Zu Beginn konfigurieren wir daher die beiden Pins, da wir sowohl Daten empfangen als auch senden wollen
	// Diese m�ssen wieder als "Alternate Function" konfiguriert werden, um mit der USART verbunden zu werden
	// Wir konfigureren beide Pins genau gleich und k�nnen dies in der Init-Struktur auch gleich kombinieren
	GPIO_InitStruct.GPIO_Mode=GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_OType=GPIO_OType_PP;
	GPIO_InitStruct.GPIO_Pin=GPIO_Pin_2 | GPIO_Pin_3;  // ODER Verkn�pfung, um beide Pins auszuw�hlen
	GPIO_InitStruct.GPIO_PuPd=GPIO_PuPd_NOPULL;
	GPIO_InitStruct.GPIO_Speed=GPIO_Speed_50MHz;

	// Struktur an GPIO Port A �bergeben und damit beide Pins konfigurieren
	GPIO_Init(GPIOA, &GPIO_InitStruct);

	// Jetzt m�ssen wir noch die richtige AF Funktion f�r beide Pins ausw�hlen, damit sie auch mit der USART2 verbunden werden
	// Im Datasheet (PDF, Kapitel 4, Seite 42) sehen wir, dass Pin A.2 und Pin A.3 als AF7 konfiguriert werden m�ssen
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_7);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_7);

	// Die Pins der USART2 sind nun konfiguriert und mit der USART verbunden
	// Nun muss die USART selbst konfiguriert werden
	// Zu Beginn m�ssen wir sie wieder aktivieren (liegt am APB1)
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

	// Auch f�r die USART gibt es wieder eine Initialisierungs-Struktur
	// Gem�� der Angaben auf TUWEL, sollen wir die USART wie folgt konfigurieren:
	USART_InitStruct.USART_BaudRate=9600;                                       // Baudrate = 9600
	USART_InitStruct.USART_WordLength=USART_WordLength_8b;                      // Datenbits = 8 ("Wortl�nge")
	USART_InitStruct.USART_StopBits=USART_StopBits_1;                           // Stopbits = 1
	USART_InitStruct.USART_Parity=USART_Parity_No;                              // kein Paritybit
	USART_InitStruct.USART_HardwareFlowControl=USART_HardwareFlowControl_None;  // keine Hardware Flow Control
	USART_InitStruct.USART_Mode=USART_Mode_Tx | USART_Mode_Rx;                  // Einstellungen gelten f�r RX und TX
																																								 // wir wollen beide Richtungen aktivieren

	// Struktur an USART 2 �bergeben
	USART_Init(USART2, &USART_InitStruct);

	// �hnlich wie bei dem Timer, muss auch hier die USART aktiviert ("gestartet") werden
	USART_Cmd(USART2, ENABLE);

	// Die USART2 ist nun konfiguriert und arbeitet auch schon
	// Jetzt wollen wir noch den Empfang von Daten mit einem Interrupt abfangen
	// So k�nnen wir die empfangenen Daten auslesen und gem�� der Aufgabenstellung zur�cksenden
	// Die USART bietet eine Vielzahl an Interruptquellen (PDF, Reference Manual, Kapitel 28.7, S. 987)
	// Wir ben�tigen den "Receive data register not empty" Interrupt (RXNE)...
	// Ein langer Name, aber er sagt nichts anderes als: "Daten sind angekommen und abholbereit"
	// Sobald ein Datenwort vollst�ndig empfangen wurde, wird es in das Receive Data Register (RDR) geschrieben
	// Dadurch wird das RXNE Interrupt Flag gesetzt und der zugeh�rige Interrupt Handler aufgerufen
	// Lesen wir die Daten aus dem RDR per Software aus, so wird das RXNE Interrupt Flag von der Hardware eigenst�ndig r�ckgesetzt
	// Erst beim n�chsten Datenempfang wird das Interrupt Flag erneut gesetzt und der Interrupt damit wieder ausgel�st
	// Dieses "eigenst�ndige" R�cksetzen des Interurpt Flags wird uns beim Interrupt Handler sp�ter noch besch�ftigen...
	USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);

	// Nun fehlt noch die NVIC Konfiguration des Interrupt Channels
	// Der zugeh�rige Interrupt Channel ist "USART2_IRQn" (siehe Startup Datei "stm32f30x.h")
	// Wir aktivieren den Channel und geben ihm eine niedrigere Priorit�t als dem EXTI Interrupt (h�here Zahl!)
	NVIC_InitStruct.NVIC_IRQChannel=USART2_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelCmd=ENABLE;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority=1;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority=0;

	// Struktur �bergeben ... fertig, jetzt wird der zugeh�rige Interrupt Handler aufgerufen, wenn Daten empfangen wurden :-)
	NVIC_Init(&NVIC_InitStruct);

}




// INITIALIZATION ROUTINE FOR THE SPI INTERFACE
void SPI_init(void)
{
	SPI_InitTypeDef  SPI_InitStructure;
  GPIO_InitTypeDef GPIO_InitStructure;
  //NVIC_InitTypeDef NVIC_InitStructure;
  
  /* Enable the SPI periph */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_SPI1, ENABLE);
  
  /* Enable SCK, MOSI, MISO and NSS GPIO clocks */
  RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
  
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource5, GPIO_AF_5);
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource7, GPIO_AF_5);
  
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
  GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;

  /* SPI SCK pin configuration */
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_5;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

  /* SPI  MOSI pin configuration */
  GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_7;
  GPIO_Init(GPIOA, &GPIO_InitStructure);

	/* SPI CS pin configuration */
  //GPIO_StructInit(&GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	GPIOA->ODR |=1<<4;  
	
    
  /* SPI configuration -------------------------------------------------------*/
  SPI_I2S_DeInit(SPI1);
  SPI_InitStructure.SPI_Direction = SPI_Direction_1Line_Tx;
  SPI_InitStructure.SPI_DataSize = SPI_DataSize_16b;
  SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
  SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
  SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
	// 500kHz with 128, 1MHz with 64 as prescaler
  SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_128; 
  SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
  SPI_InitStructure.SPI_CRCPolynomial = 7;
  
  /* Initializes the SPI communication */
  SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
  SPI_Init(SPI1, &SPI_InitStructure);
  
  /* Enable the SPI peripheral */
  SPI_Cmd(SPI1, ENABLE);
	
	
}






void ADC_init(void){
	GPIO_InitTypeDef    GPIO_InitStructure;
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseInitStructure;
	

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	// Variable f�r den Kalibrierungs-Wert
	uint32_t calibration_value = 0;
	

	// Beim ADC gibt es zwei Init-Strukturen
	// common Struktur beinhaltet gemeinsame Parameter der beiden ADCs (1,2)
	// "normale" Init-Struktur beinhaltet Parameter f�r jeden ADC individuell
	ADC_CommonInitTypeDef    ADC_CommonInitStructure;
	ADC_InitTypeDef          ADC_InitStructure;
	
	// Takt f�r den ADC freigeben; ADC 1 und 2 liegen bei am AHB --> nur gemeinsam aktivieren
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_ADC12, ENABLE);
 
	// Takt entweder von "ADC12_CK" (PLL) oder vom AHB
	// "RCC_ADC12PLLCLK_OFF" --> verwenden nicht Takt vom PLL 
	RCC_ADCCLKConfig(RCC_ADC12PLLCLK_OFF);
	
	// starten mit common Konfiguration --> befuellen mit default-Werte
	ADC_CommonStructInit(&ADC_CommonInitStructure);
	
	// verwenden AHB Takt --> "ADC_Clock_SynClkModeDiv1" wird der AHB Bus als Taktquelle ausgew�hlt 
	// (dividiert durch 1, es w�re auch noch /2 oder /4 m�glich)
	ADC_CommonInitStructure.ADC_Clock = ADC_Clock_SynClkModeDiv1; //--> 64MHz
	
	// uebergeben Struktur
	// Aber warum ist hier das angeben des ADCs wichtig, obwohl es ja "gemeinsame" Parameter sind?
	// Das liegt an der Firmware Library, die so entworfen wurde, dass sie f�r mehrere verschiedene Controller funktioniert
	// Manche haben 4 ADCs und bei diesen gibt es dann gemeinsame Einstellungen f�r ADC1-2 und gemeinsame Einstellungen f�r ADC3-4
	// Daher will die Funktion die ADC Nummer wissen, obwohl es beim STM32F334R8 die "common" Register eh nur einmal gibt 
	ADC_CommonInit(ADC1, &ADC_CommonInitStructure);
	
	
	
	
	// weiter mit Konfiguration des ADC1
	ADC_StructInit(&ADC_InitStructure); // sample resolution 12b
	
	// wollen kontinuierliche Messung
	ADC_InitStructure.ADC_ContinuousConvMode = ADC_ContinuousConvMode_Enable;
	// 2)"Overrun" Modus --> ADC misst weiter und �berschreibt den alten Messwert
	ADC_InitStructure.ADC_OverrunMode = ADC_OverrunMode_Enable;
	
	// uebergeben der Struktur
	ADC_Init(ADC1, &ADC_InitStructure);
	
	
	// ADC Channels festlegen welche der ADC1 messen soll
	// und eine beliebige Reihenfolge festlegen ("Channel Sequence",siehe VO)
	// 1) Welchen ADC wollen wir konfigurieren --> ADC1
	// 2) Welchen Channel wollen wir in die "Channel Sequence" Liste einf�gen
	// 3) An welcher Stelle in der "Channel Sequence" Liste soll der Channel stehen
	// 4) Welche Sample Time soll f�r den Channel eingestellt werden
	ADC_RegularChannelConfig(ADC1, ADC_Channel_1, 1, ADC_SampleTime_181Cycles5);
	ADC_RegularChannelConfig(ADC1, ADC_Channel_2, 2, ADC_SampleTime_181Cycles5);
	
	
	
	// Anzahl an Channels in unserer "Channel Sequence" Liste angeben
	ADC_RegularChannelSequencerLengthConfig(ADC1,2);
	
	
	// "Voltage Regulator" aktivieren
	ADC_VoltageRegulatorCmd(ADC1, ENABLE);
	
	// mindestens 10us warten, bevor wir den ADC kalibrieren oder starten k�nnen
	// es gibt leider kein Flag
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE );

	TIM_TimeBaseStructInit(&TIM_TimeBaseInitStructure);
	TIM_TimeBaseInitStructure.TIM_Prescaler=31999;         // 32 MHz Takt dividiert durch 8000 -> 1kHz Z�hlfrequenz                               
	TIM_TimeBaseInitStructure.TIM_Period= 99;           // Von 0 bis 99 z�hlen
	TIM_TimeBaseInit(TIM2,&TIM_TimeBaseInitStructure);
	
	TIM_ITConfig(TIM2, TIM_IT_Update, ENABLE);
	// nutzen TIM2 um zu warten
	TIM_Cmd(TIM2, ENABLE);
	TIM_ClearITPendingBit(TIM2,TIM_IT_Update);	
	
	// Dann sind 2 Sekunden vorbei, mehr als genug Zeit f�r den ADC Voltage Regulator
	while(TIM_GetFlagStatus(TIM2,TIM_IT_Update)==0){};
	TIM_Cmd(TIM2, DISABLE);
	TIM_ClearITPendingBit(TIM2,TIM_IT_Update);	
	NVIC_ClearPendingIRQ(TIM2_IRQn); // clearn pending bit, sonst naechstes mal gleich aktiviert
	
	
	// Weiter geht es mit dem ADC und seiner "Aktivierung"			
	// starten Kalibrierung (damit wir m�glichst genaue Werte erhalten)
	// Art der Kalibrierung bestimmen"ADC_SelectCalibrationMode"
	// verwenden "ADC_CalibrationMode_Single", da nur 1 ADC
	ADC_SelectCalibrationMode(ADC1, ADC_CalibrationMode_Single);
	ADC_StartCalibration(ADC1);

	//warten bis Kalibrierung fertig; auf Flag "ADC_GetCalibrationStatus"
	while(ADC_GetCalibrationStatus(ADC1) != RESET );
	
	// muessten hier 4 taktzyklen warten, was wir hoffentlich mit dem auslesen
	// des calibration_value erreichen
	calibration_value = ADC_GetCalibrationValue(ADC1);
	 
	ADC_ITConfig(ADC1, ADC_IT_EOS, ENABLE);

		
	// starten ADC 
	ADC_Cmd(ADC1, ENABLE);
	
	// warten bis fertig gestartet --> ADC_FLAG_RDY == Set
	while(!ADC_GetFlagStatus(ADC1, ADC_FLAG_RDY));



	// Messvorgang (Conversion) starten, damit der ADC beginnt die Channels zu messen
	ADC_StartConversion(ADC1);   
	
	
	// Jetzt sind wir soweit, dass wir den Timer 2 im gew�nschten Modus (Interrupt alle 2 Sekunden) starten k�nnen
	// Wir m�ssen dazu noch den zugeh�rigen Interrupt Channel aktivieren und den Timer anschlie�end starten
	
	// Init-Struktur
	NVIC_InitTypeDef NVIC_InitStructure;

	// Interrupt Channel f�r Timer 2 konfigurieren
	NVIC_InitStructure.NVIC_IRQChannel=ADC1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelCmd=ENABLE;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority=1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority=0;
	NVIC_Init(&NVIC_InitStructure);

	// Timer 2 starten	
	//TIM_Cmd(TIM2, ENABLE);
		
}




void TIM3_init(void){
	
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure; 
	NVIC_InitTypeDef NVIC_NVICInitStructure; 

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

	TIM_TimeBaseStructInit(&TIM_TimeBaseInitStructure);
	
	
	// Bustakt 32 MHz / Prescaler (Registerwert + 1) dividiert.																						
	TIM_TimeBaseInitStructure.TIM_Prescaler=31999; 
	// Autoreload value: counts to number, if reached there is an update event
	TIM_TimeBaseInitStructure.TIM_Period= 100;    
	
	// Jetzt �bergeben wir die Struktur an den Timer 3:
	TIM_TimeBaseInit(TIM3,&TIM_TimeBaseInitStructure);
	
	
	// configure NVIC for interrupt
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	NVIC_NVICInitStructure.NVIC_IRQChannel=TIM3_IRQn;           // TIM3 Channel
	NVIC_NVICInitStructure.NVIC_IRQChannelCmd=ENABLE;           // nat�rlich aktivieren wir den Interrupt Channel
	NVIC_NVICInitStructure.NVIC_IRQChannelPreemptionPriority=0; // Gruppenpriorit�t    (0=h�chste)
	NVIC_NVICInitStructure.NVIC_IRQChannelSubPriority=0;        // Subgruppenpriorit�t (0=h�chste)
	
	// Auch hier m�ssen wir die Struktur wieder an den NVIC �bergeben, damit die Register entsprechend beschrieben werden.
	NVIC_Init(&NVIC_NVICInitStructure);

	// Zur�ck zu Timer 3: Dieser ist zwar konfiguriert, er l�uft aber noch nicht.
	// Wir m�ssen ihn also noch starten und das geschieht mit dem Befehl:
	TIM_Cmd(TIM3, ENABLE); 

	// Wir haben schon den Interrupt Channel von TIM3 im NVIC aktiviert.
	// Jetzt m�ssen wir in der Konfiguration von TIM3 aber auch noch den "Update Interrupt" aktivieren.
	// Erst wenn im Timer der Update Interrupt und in der NVIC der zugeh�rige Channel aktiviert ist, wird der Interrupt auch ausgel�st.
	//TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);
}

