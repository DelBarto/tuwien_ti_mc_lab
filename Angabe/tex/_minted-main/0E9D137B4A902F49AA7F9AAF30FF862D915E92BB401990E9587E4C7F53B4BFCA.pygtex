\begin{Verbatim}[commandchars=\\\{\}]
\PYG{k+kt}{void} \PYG{n+nf}{USART2\PYGZus{}IRQHandler}\PYG{p}{()}
\PYG{p}{\PYGZob{}}
    \PYG{c+c1}{// Zur Erinnerung: Wir haben den RXNE Interrupt aktiviert, der Handler wird}
    \PYG{c+c1}{// also aufgerufen, wenn ein Datenwort empfangen wurde}

    \PYG{c+c1}{// Wir werden eine Variable benötigen, in der wir das empfangene Datenwort}
    \PYG{c+c1}{// zwischenspeichern}
    \PYG{k+kt}{int} \PYG{n}{recvd}\PYG{p}{;}

    \PYG{c+c1}{// Wir lesen die empfangenen Daten aus dem RDR aus und speichern sie in}
    \PYG{c+c1}{// unserer Variable}
    \PYG{c+c1}{// Genau jetzt setzt die Hardware das RXNE Interrupt Flag eingeständig}
    \PYG{c+c1}{// zurück und die USART ist bereit neue Daten zu empfangen}
    \PYG{c+c1}{// (PDF, Reference Manual, Kapitel 28.5.3, S. 956)}
    \PYG{c+c1}{// Daher ist es auch ratsam, die empfangenden Daten in einer Variable zu}
    \PYG{c+c1}{// speichern und ab dann nur mehr diese zu verwenden}
    \PYG{c+c1}{// Schließlich können ab jetzt neue Daten empfangen werden, die von der}
    \PYG{c+c1}{// Hardware ins RDR Register geschrieben werden}
    \PYG{c+c1}{// Wenn wir zu lange \PYGZdq{}herumrechnen\PYGZdq{} und nochmals aus dem RDR lesen, stehen}
    \PYG{c+c1}{// dort also vielleicht schon die neueren Daten}
    \PYG{n}{recvd}\PYG{o}{=}\PYG{n}{USART\PYGZus{}ReceiveData}\PYG{p}{(}\PYG{n}{USART2}\PYG{p}{);}


    \PYG{c+c1}{//  Wir prüfen, ob es sich bei dem empfangenen Datenwort um einen}
    \PYG{c+c1}{// Großbuchstaben handelt}
    \PYG{k}{if} \PYG{p}{((}\PYG{n}{recvd}\PYG{o}{\PYGZgt{}=}\PYG{l+s+sc}{\PYGZsq{}A\PYGZsq{}}\PYG{p}{)}\PYG{o}{\PYGZam{}\PYGZam{}}\PYG{p}{(}\PYG{n}{recvd}\PYG{o}{\PYGZlt{}=}\PYG{l+s+sc}{\PYGZsq{}Z\PYGZsq{}}\PYG{p}{))\PYGZob{}}

        \PYG{c+c1}{// Haben wir einen Großbuchstaben empfangen, wollen wir den selben}
        \PYG{c+c1}{// Buchstaben in klein zurücksenden}
        \PYG{c+c1}{// Bevor wir jetzt einfach Daten in das Senderegister (Transmit Data}
        \PYG{c+c1}{// Register, TDR) schreiben, müssen wir erst prüfen,}
        \PYG{c+c1}{// ob die USART nicht gerade damit beschäftigt ist, andere Daten zu}
        \PYG{c+c1}{// senden (z.B. von unserem Taster)}
        \PYG{c+c1}{// Dazu nützen wir das TXE Flag der USART (PDF, Reference Manual,}
        \PYG{c+c1}{// Kapitel 28.5.2, S. 953)}
	\PYG{c+c1}{// Es wird von der Hardware gesetzt, wenn das TDR leer ist und mit neuen}
  \PYG{c+c1}{// Daten zum Senden gefüllt werden kann}
        \PYG{c+c1}{// Wir prüfen also das TXE Flag und warten solange, bis es gesetzt ist}
        \PYG{k}{while}\PYG{p}{(}\PYG{n}{USART\PYGZus{}GetFlagStatus}\PYG{p}{(}\PYG{n}{USART2}\PYG{p}{,} \PYG{n}{USART\PYGZus{}FLAG\PYGZus{}TXE}\PYG{p}{)}\PYG{o}{!=}\PYG{n}{SET}\PYG{p}{)\PYGZob{}\PYGZcb{};}

        \PYG{c+c1}{// Jetzt ist die USART sicher bereit ...}
        \PYG{c+c1}{// Wir schreiben also den entsprechenden Kleinbuchstaben in das TDR}
        \PYG{c+c1}{// und die USART versendet die Daten für uns}
        \PYG{c+c1}{// Im ASCII Code ist der Kleinbuchstabe genau 32 Zeichen über dem}
        \PYG{c+c1}{// zugehörigen Großbuchstaben \PYGZhy{}\PYGZgt{} \PYGZdq{}revcd+32\PYGZdq{}}
        \PYG{n}{USART\PYGZus{}SendData}\PYG{p}{(}\PYG{n}{USART2}\PYG{p}{,} \PYG{n}{recvd}\PYG{o}{+}\PYG{l+m+mi}{32}\PYG{p}{);}
    \PYG{p}{\PYGZcb{}}

    \PYG{c+c1}{// Jetzt das gleiche für Kleinbuchstaben ...}
    \PYG{k}{if} \PYG{p}{((}\PYG{n}{recvd}\PYG{o}{\PYGZgt{}=}\PYG{l+s+sc}{\PYGZsq{}a\PYGZsq{}}\PYG{p}{)}\PYG{o}{\PYGZam{}\PYGZam{}}\PYG{p}{(}\PYG{n}{recvd}\PYG{o}{\PYGZlt{}=}\PYG{l+s+sc}{\PYGZsq{}z\PYGZsq{}}\PYG{p}{))\PYGZob{}}
        \PYG{k}{while}\PYG{p}{(}\PYG{n}{USART\PYGZus{}GetFlagStatus}\PYG{p}{(}\PYG{n}{USART2}\PYG{p}{,} \PYG{n}{USART\PYGZus{}FLAG\PYGZus{}TXE}\PYG{p}{)}\PYG{o}{!=}\PYG{n}{SET}\PYG{p}{)\PYGZob{}\PYGZcb{};}
        \PYG{n}{USART\PYGZus{}SendData}\PYG{p}{(}\PYG{n}{USART2}\PYG{p}{,} \PYG{n}{recvd}\PYG{o}{\PYGZhy{}}\PYG{l+m+mi}{32}\PYG{p}{);}
    \PYG{p}{\PYGZcb{}}

    \PYG{c+c1}{// Hier folgt normalerweise noch das Rücksetzen des Pending Bits (in}
    \PYG{c+c1}{// diesem Fall das RXNE Interrupt Flag)}
    \PYG{c+c1}{// Das ist dieses Mal aber nicht nötig, denn das hat ja bereits die}
    \PYG{c+c1}{// Hardware für uns erledigt!}
    \PYG{c+c1}{// Das Rücksetzen ist genau genommen nicht nur \PYGZdq{}nicht NÖTIG\PYGZdq{}, es ist gar}
    \PYG{c+c1}{// NICHT MÖGLICH und streng genommen gar NICHT ERLAUBT}
    \PYG{c+c1}{// Schauen wir uns die USART Register etwas genauer an (PDF, Reference}
    \PYG{c+c1}{// Manual, Kapitel 28.8, S. 1003\PYGZhy{}1009), so fällt auf:}
    \PYG{c+c1}{// 1) Im Interrupt and Status Register (ISR) können wir uns den aktuellen}
    \PYG{c+c1}{// Zustand des RXNE Flags zwar anschauen}
    \PYG{c+c1}{// 2) Im Interrupt Flag Clear Register (ICR) können wir das RXNE Flag aber}
    \PYG{c+c1}{// gar nicht rücksetzen, das Bit darf gar nicht beschrieben werden}
    \PYG{c+c1}{// Dieses Flag kann also nur die Hardware rücksetzen}
    \PYG{c+c1}{// Und das macht sie jedes Mal, wenn wir mit unserer Software lesend auf}
    \PYG{c+c1}{// das RDR Register zugreifen}
    \PYG{c+c1}{// USART\PYGZus{}ClearITPendingBit(USART2, USART\PYGZus{}IT\PYGZus{}RXNE) ... brauchen wir also}
    \PYG{c+c1}{// nicht, das macht die Hardware :\PYGZhy{})}
\PYG{p}{\PYGZcb{}}
\end{Verbatim}
