\begin{Verbatim}[commandchars=\\\{\}]
\PYG{c+cm}{/****************** Schritt 3: Interrupt Handler von Timer 2 ******************/}
\PYG{k+kt}{void} \PYG{n+nf}{TIM2\PYGZus{}IRQHandler}\PYG{p}{(}\PYG{k+kt}{void}\PYG{p}{)}
\PYG{p}{\PYGZob{}}
   \PYG{c+c1}{// Die LED2 ist am Pin 5 von Port A angeschlossen.}
   \PYG{c+c1}{// Wenn wir daher das 5te Bit im ODR Register von GPIO Port A setzen (=\PYGZsq{}1\PYGZsq{}),}
   \PYG{c+c1}{// dann leuchtet die LED.}
   \PYG{c+c1}{// Setzen wir das Bit wieder zur\PYGZbs{}\PYGZdq{}uck (=\PYGZsq{}0\PYGZsq{}), h\PYGZbs{}\PYGZdq{}ort die LED auf zu leuchten.}
   \PYG{c+c1}{// Da diese Routine jede Sekunde aufgerufen wird, wollen wir das Bit}
   \PYG{c+c1}{// abwechseln setzen und r\PYGZbs{}\PYGZdq{}ucksetzen, damit die LED blinkt.}
   \PYG{c+c1}{// Wir wollen das Bit also bei jedem Funktionsaufruf toggeln .... dann blinkt}
   \PYG{c+c1}{//  die LED im Sekundentakt.}
   \PYG{c+c1}{// Und das erreichen wir z.B. mit einer XOR Verkn\PYGZbs{}\PYGZdq{}upfung ( `\PYGZca{}\PYGZsq{} ist das}
   \PYG{c+c1}{// Symbol f\PYGZbs{}\PYGZdq{}ur die XOR Verkn\PYGZbs{}\PYGZdq{}upfung).}
   \PYG{c+c1}{// 1\PYGZca{}1=0 und 0\PYGZca{}1=1 \PYGZhy{}\PYGZgt{} Wir m\PYGZbs{}\PYGZdq{}ussen das ODR Bit also mit `1\PYGZsq{} XOR verkn\PYGZbs{}\PYGZdq{}upfen,}
   \PYG{c+c1}{// um dessen Inhalt zu toggeln .}

   \PYG{n}{GPIOA}\PYG{o}{\PYGZhy{}\PYGZgt{}}\PYG{n}{ODR} \PYG{o}{\PYGZca{}=} \PYG{l+m+mi}{1}\PYG{o}{\PYGZlt{}\PYGZlt{}}\PYG{l+m+mi}{5}\PYG{p}{;}  \PYG{c+c1}{// LED blinkt}

   \PYG{c+c1}{// Wir sind noch nicht ganz fertig. Ein letzter sehr wichtiger Schritt fehlt}
   \PYG{c+c1}{// noch:}
   \PYG{c+c1}{// Der Interrupt wurde aufgerufen und unser Code ausgef\PYGZbs{}\PYGZdq{}uhrt.}
   \PYG{c+c1}{// Jetzt m\PYGZbs{}\PYGZdq{}ussen wir dem Controller mitteilen, dass wir den aufgetretenen}
   \PYG{c+c1}{// Interrupt fertig abgearbeitet haben.}
   \PYG{c+c1}{// Dies tun wir, indem wir das Interrupt Pending Bit des Interrupts wieder}
   \PYG{c+c1}{// zur\PYGZbs{}\PYGZdq{}ucksetzen.}
   \PYG{c+c1}{// Dazu verwenden wir die Funktion \PYGZdq{}TIM\PYGZus{}ClearITPendingBit\PYGZdq{}.}
   \PYG{c+c1}{// Ihr \PYGZbs{}\PYGZdq{}ubergeben wir die Peripherie und den Namen des Interrupts, von}
   \PYG{c+c1}{// welchem wir das Pending Bit zur\PYGZbs{}\PYGZdq{}ucksetzen wollen.}
   \PYG{c+c1}{// In unserem Fall also der Update Interrupt von TIM2.}
   \PYG{n}{TIM\PYGZus{}ClearITPendingBit}\PYG{p}{(}\PYG{n}{TIM2}\PYG{p}{,}\PYG{n}{TIM\PYGZus{}IT\PYGZus{}Update}\PYG{p}{);}

   \PYG{c+c1}{// Dieser letzte Schritt ist sehr wichtig, da das Pending Bit sonst gesetzt}
   \PYG{c+c1}{// bleibt.}
   \PYG{c+c1}{// Bleibt es gesetzt, so wird dem NVIC dadurch nach der Abarbeitung des}
   \PYG{c+c1}{// Interrupt Handlers sofort wieder signalisiert, dass der Interrupt}
   \PYG{c+c1}{// ausgel\PYGZbs{}\PYGZdq{}ost wurde.}
   \PYG{c+c1}{// Der Interrupt Handler w\PYGZbs{}\PYGZdq{}urde also sofort wieder aufgerufen werden ...}
   \PYG{c+c1}{// und wieder ... und wieder ... (außer ein h\PYGZbs{}\PYGZdq{}oher priorisierter Interrupt}
   \PYG{c+c1}{// tritt auf).}
\PYG{p}{\PYGZcb{}}
\end{Verbatim}
