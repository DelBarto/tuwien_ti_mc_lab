\begin{Verbatim}[commandchars=\\\{\}]
\PYG{k+kt}{int} \PYG{n+nf}{main}\PYG{p}{(}\PYG{k+kt}{void}\PYG{p}{)}
\PYG{p}{\PYGZob{}}
    \PYG{c+cm}{/*}
\PYG{c+cm}{    Hier werden wir das Porgramm in zwei Schritte unterteilen:}
\PYG{c+cm}{       1. Konfiguration von GPIO Pin A.5}
\PYG{c+cm}{       2. Konfiguration von Timer 2 (PWM)}
\PYG{c+cm}{    */}

    \PYG{c+cm}{/*}
\PYG{c+cm}{     An dieser Stelle (Start der \PYGZdq{}main\PYGZdq{} Funktion) sind die Startup\PYGZhy{}Dateien}
\PYG{c+cm}{     bereits ausgeführt.}
\PYG{c+cm}{     Der Systemtakt und die einzelnen Bus\PYGZhy{}Takte sind also bereits konfiguriert.}
\PYG{c+cm}{     Die genauen Einstellungen sind in den Kommentaren der Startup\PYGZhy{}Datei}
\PYG{c+cm}{     \PYGZdq{}system\PYGZus{}stm32f3xx.c\PYGZdq{} ersichtlich.}
\PYG{c+cm}{     In unserem Fall ist der Systemtakt 8MHz und auch der Takt der Peripheral}
\PYG{c+cm}{     Busse ist auf 8MHz gesetzt}
\PYG{c+cm}{    */}

    \PYG{c+c1}{// Dies sind die Initialisierungs\PYGZhy{}Strukuren von GPIO und TIM2, wie wir sie}
    \PYG{c+c1}{// bereits kennen}
    \PYG{n}{GPIO\PYGZus{}InitTypeDef} \PYG{n}{GPIO\PYGZus{}InitTypeStructure}\PYG{p}{;}
    \PYG{n}{TIM\PYGZus{}TimeBaseInitTypeDef} \PYG{n}{TIM\PYGZus{}TimeBaseInitStructure}\PYG{p}{;}

    \PYG{c+c1}{// Neben der \PYGZdq{}Basis\PYGZhy{}Timer\PYGZhy{}Konfiguration\PYGZdq{} müssen wir nun auch den für die}
    \PYG{c+c1}{// PWM zuständigen Teil des TIM2 konfigurieren}
    \PYG{c+c1}{// Die PWM ist Teil des \PYGZdq{}Output Compare\PYGZdq{} Modus von TIM2 und daher benötigen}
    \PYG{c+c1}{// wir zusätzlich eine \PYGZdq{}OCInit\PYGZdq{} Struktur}
    \PYG{n}{TIM\PYGZus{}OCInitTypeDef} \PYG{n}{TIM\PYGZus{}OCInitStructure}\PYG{p}{;}


    \PYG{c+cm}{/*********** Schritt 1: Konfiguration von Pin A.5 für die LED ************/}

    \PYG{c+c1}{// Wir rufen die Funktion \PYGZdq{}RCC\PYGZus{}AHBPeriphClockCmd\PYGZdq{} auf und aktivieren den}
    \PYG{c+c1}{// Takt für GPIO Port A}
    \PYG{n}{RCC\PYGZus{}AHBPeriphClockCmd}\PYG{p}{(}\PYG{n}{RCC\PYGZus{}AHBPeriph\PYGZus{}GPIOA}\PYG{p}{,} \PYG{n}{ENABLE}\PYG{p}{);}

    \PYG{c+c1}{// Dieses Mal konfigurieren wir den Pin NICHT als Ausgang sondern als}
    \PYG{c+c1}{// \PYGZdq{}Alternate Function\PYGZdq{}}
    \PYG{c+c1}{// Die restlichen Parameter lassen wir gleich, schließlich soll der Pin}
    \PYG{c+c1}{// immer noch ein Push\PYGZhy{}Pull Ausgang ohne Pullup\PYGZhy{} oder Pulldown Widerstand}
    \PYG{c+c1}{// sein}
    \PYG{c+c1}{// Die Ausgangsstufe (Output Driver) des Pins wird nun aber nicht mehr von}
    \PYG{c+c1}{// uns über das ODR Register angesteuert}
    \PYG{c+c1}{// Stattdessen verbinden wir sie direkt mit einem PWM Ausgang von TIM2}
    \PYG{c+c1}{// (= \PYGZdq{}Alternate Function\PYGZdq{} Modus)}

    \PYG{n}{GPIO\PYGZus{}InitTypeStructure}\PYG{p}{.}\PYG{n}{GPIO\PYGZus{}Mode}\PYG{o}{=}\PYG{n}{GPIO\PYGZus{}Mode\PYGZus{}AF}\PYG{p}{;}       \PYG{c+c1}{// Das ist neu!}
    \PYG{n}{GPIO\PYGZus{}InitTypeStructure}\PYG{p}{.}\PYG{n}{GPIO\PYGZus{}OType}\PYG{o}{=}\PYG{n}{GPIO\PYGZus{}OType\PYGZus{}PP}\PYG{p}{;}
    \PYG{n}{GPIO\PYGZus{}InitTypeStructure}\PYG{p}{.}\PYG{n}{GPIO\PYGZus{}Pin}\PYG{o}{=}\PYG{n}{GPIO\PYGZus{}Pin\PYGZus{}5}\PYG{p}{;}
    \PYG{n}{GPIO\PYGZus{}InitTypeStructure}\PYG{p}{.}\PYG{n}{GPIO\PYGZus{}PuPd}\PYG{o}{=}\PYG{n}{GPIO\PYGZus{}PuPd\PYGZus{}NOPULL}\PYG{p}{;}
    \PYG{n}{GPIO\PYGZus{}InitTypeStructure}\PYG{p}{.}\PYG{n}{GPIO\PYGZus{}Speed}\PYG{o}{=}\PYG{n}{GPIO\PYGZus{}Speed\PYGZus{}50MHz}\PYG{p}{;} \PYG{c+c1}{// Da wir keine hohen}
                                                        \PYG{c+c1}{// PWM Frequenzen}
                                                        \PYG{c+c1}{// erzeugen, ist der}
                                                        \PYG{c+c1}{// Speed hier immer}
                                                        \PYG{c+c1}{// noch egal}

    \PYG{c+c1}{// Jetzt senden wir die Struktur an Port A}
    \PYG{n}{GPIO\PYGZus{}Init}\PYG{p}{(}\PYG{n}{GPIOA}\PYG{p}{,} \PYG{o}{\PYGZam{}}\PYG{n}{GPIO\PYGZus{}InitTypeStructure}\PYG{p}{);}

    \PYG{c+c1}{// Pin A.5 ist nun als \PYGZdq{}Alternate Function\PYGZdq{} (AF) Pin konfiguriert}
    \PYG{c+c1}{// Die meisten Pins des STM32 können jedoch für mehrere verschiedene}
    \PYG{c+c1}{// \PYGZdq{}Alternate Functions\PYGZdq{} verwendet werden}
    \PYG{c+c1}{// Wir müssen also noch die korrekte AF auswählen, damit der Pin auch mit}
    \PYG{c+c1}{// dem PWM Ausgang von TIM2 verbunden wird}
    \PYG{c+c1}{// Im Datasheet (PDF Seite 42\PYGZhy{}43) finden wir eine Tabelle, in der die}
    \PYG{c+c1}{// möglichen AFs für alle Pins aufgelistet sind}
    \PYG{c+c1}{// Wir sehen in der Tabelle, dass Pin A.5 im AF\PYGZhy{}Modus unter anderem als}
    \PYG{c+c1}{// \PYGZdq{}TIM2\PYGZus{}CH1\PYGZdq{} verwendet werden kann und dazu als \PYGZdq{}AF1\PYGZdq{} konfiguriert werden}
    \PYG{c+c1}{// muss \PYGZdq{}TIM2\PYGZus{}CH1\PYGZdq{} ist der interne Kanal 1 (Channel 1) von TIM2 (siehe VO}
    \PYG{c+c1}{// bzw. Reference Manual)}
    \PYG{c+c1}{// Wenn wir das PWM Signal des Timers auf seinen internen Channel 1 legen}
    \PYG{c+c1}{// (in Schritt 2) und den Pin A.5 als \PYGZdq{}AF1\PYGZdq{} konfigurieren, so wird die PWM}
    \PYG{c+c1}{// an den Ausgang geleitet}
    \PYG{c+c1}{// Wir müssen den Pin A.5 also noch zusätzlich so konfigurieren, dass er}
    \PYG{c+c1}{// im \PYGZdq{}AF1\PYGZdq{} Modus arbeitet}
    \PYG{c+c1}{// Dazu verwenden wir die Funktion \PYGZdq{}GPIO\PYGZus{}PinAFConfig\PYGZdq{}}
    \PYG{n}{GPIO\PYGZus{}PinAFConfig}\PYG{p}{(}\PYG{n}{GPIOA}\PYG{p}{,} \PYG{n}{GPIO\PYGZus{}PinSource5}\PYG{p}{,} \PYG{n}{GPIO\PYGZus{}AF\PYGZus{}1}\PYG{p}{);}


    \PYG{c+cm}{/***************** Schritt 2: Timer 2 (PWM) Konfigurieren *****************/}

    \PYG{c+c1}{// Auch hier müssen wir zu Beginn wieder den Takt für TIM2 aktivieren}
    \PYG{n}{RCC\PYGZus{}APB1PeriphClockCmd}\PYG{p}{(}\PYG{n}{RCC\PYGZus{}APB1Periph\PYGZus{}TIM2}\PYG{p}{,} \PYG{n}{ENABLE}\PYG{p}{);}

    \PYG{c+c1}{// Für die Konfiguration des PWM\PYGZhy{}Signals benötigen wir zwei}
    \PYG{c+c1}{// Zeitinformationen}
    \PYG{c+c1}{// 1. Wie lange dauert ein Periodendauer}
    \PYG{c+c1}{// 2. Wie groß ist die Pulsweite}

    \PYG{c+c1}{// Die Periodendauer des PWM Signals wird durch das}
    \PYG{c+c1}{// Autoreload Register (ARR) bestimmt}
    \PYG{c+c1}{// Der Timer zählt von 0 bis zu dem ARR Wert und fängt dann von vorne}
    \PYG{c+c1}{// an \PYGZhy{}\PYGZgt{} Periodendauer wird durch Autoreload Wert bestimmt}
    \PYG{c+c1}{// Während des Zählens vergleicht der Timer den Zählstand mit dem Wert im}
    \PYG{c+c1}{// Capture/Compare Register (CCR)}
    \PYG{c+c1}{// Wird dieser Wert überschritten, so toggelt der Ausgang \PYGZhy{}\PYGZgt{} Pulsweite wird}
    \PYG{c+c1}{// durch den CCR Wert bestimmt (der CCR Wert muss also kleiner als der}
    \PYG{c+c1}{//  ARR Wert sein)}

    \PYG{c+c1}{// Wie in Beispiel 2A konfigurieren wir zu Beginn die Basis des TIM2}
    \PYG{n}{TIM\PYGZus{}TimeBaseStructInit}\PYG{p}{(}\PYG{o}{\PYGZam{}}\PYG{n}{TIM\PYGZus{}TimeBaseInitStructure}\PYG{p}{);}
    \PYG{n}{TIM\PYGZus{}TimeBaseInitStructure}\PYG{p}{.}\PYG{n}{TIM\PYGZus{}Prescaler}\PYG{o}{=}\PYG{l+m+mi}{7999}\PYG{p}{;}   \PYG{c+c1}{// Der Bustakt (8 MHz) wird}
                                                    \PYG{c+c1}{// durch den Prescaler}
                                                    \PYG{c+c1}{// (Registerwert + 1)}
                                                    \PYG{c+c1}{// dividiert}
                                                    \PYG{c+c1}{// Wir dividieren den Takt}
                                                    \PYG{c+c1}{// also durch 8000 und}
                                                    \PYG{c+c1}{// erhalten damit 1kHz}
                                                    \PYG{c+c1}{// \PYGZdq{}Zählfrequenz\PYGZdq{}}
    \PYG{n}{TIM\PYGZus{}TimeBaseInitStructure}\PYG{p}{.}\PYG{n}{TIM\PYGZus{}Period}\PYG{o}{=} \PYG{l+m+mi}{999}\PYG{p}{;}      \PYG{c+c1}{// Der Timer zählt bis zu}
                                                    \PYG{c+c1}{// dem Autoreload Wert und}
                                                    \PYG{c+c1}{// fängt von vorne an}
                                                    \PYG{c+c1}{// Die Periodendauer}
                                                    \PYG{c+c1}{// unseres PWM Signals}
                                                    \PYG{c+c1}{// beträgt daher 1s}

    \PYG{c+c1}{// Jetzt übergeben wir die Struktur an den Timer 2}
    \PYG{n}{TIM\PYGZus{}TimeBaseInit}\PYG{p}{(}\PYG{n}{TIM2}\PYG{p}{,}\PYG{o}{\PYGZam{}}\PYG{n}{TIM\PYGZus{}TimeBaseInitStructure}\PYG{p}{);}

    \PYG{c+c1}{// Die Timer\PYGZhy{}Basis ist konfiguriert, jetzt müssen wir noch den PWM\PYGZhy{}Modus}
    \PYG{c+c1}{// des Timers aktivieren und die Pulsweite einstellen}
    \PYG{c+c1}{// Dies geschieht mittels des Output Compare (OC) Modus des Timers und}
    \PYG{c+c1}{// daher verwenden wir die \PYGZdq{}OCInit\PYGZdq{} Struktur}
    \PYG{c+c1}{// Auch hier initialisieren wir die Struktur wieder mit den Default\PYGZhy{}Werten,}
    \PYG{c+c1}{// bevor wir die relevanten Parameter einstellen (die Funktion dazu heißt}
    \PYG{c+c1}{// \PYGZdq{}TIM\PYGZus{}OCStructInit\PYGZdq{}).}
	  \PYG{n}{TIM\PYGZus{}OCStructInit}\PYG{p}{(}\PYG{o}{\PYGZam{}}\PYG{n}{TIM\PYGZus{}OCInitStructure}\PYG{p}{);}
    \PYG{n}{TIM\PYGZus{}OCInitStructure}\PYG{p}{.}\PYG{n}{TIM\PYGZus{}Pulse}\PYG{o}{=}\PYG{l+m+mi}{500}\PYG{p}{;}                \PYG{c+c1}{// Pulsweite: Jener Wert,}
                                                      \PYG{c+c1}{// der in das CCR Register}
                                                      \PYG{c+c1}{//  geschrieben wird}
                                                      \PYG{c+c1}{// (= 0.5s)}
    \PYG{n}{TIM\PYGZus{}OCInitStructure}\PYG{p}{.}\PYG{n}{TIM\PYGZus{}OCMode}\PYG{o}{=}\PYG{n}{TIM\PYGZus{}OCMode\PYGZus{}PWM1}\PYG{p}{;}   \PYG{c+c1}{// PWM Modus 1: PWM}
                                                      \PYG{c+c1}{// Channel ist \PYGZdq{}activ\PYGZdq{}}
                                                      \PYG{c+c1}{// solange der aktuelle}
                                                      \PYG{c+c1}{// Zählstand kleiner als}
                                                      \PYG{c+c1}{// der CCR\PYGZhy{}Wert ist}
    \PYG{n}{TIM\PYGZus{}OCInitStructure}\PYG{p}{.}\PYG{n}{TIM\PYGZus{}OutputState}\PYG{o}{=}\PYG{n}{TIM\PYGZus{}OutputState\PYGZus{}Enable}\PYG{p}{;}
                                                      \PYG{c+c1}{// PWM Ausgang soll}
                                                      \PYG{c+c1}{// aktiviert werden}
    \PYG{n}{TIM\PYGZus{}OCInitStructure}\PYG{p}{.}\PYG{n}{TIM\PYGZus{}OCPolarity}\PYG{o}{=}\PYG{n}{TIM\PYGZus{}OCPolarity\PYGZus{}High}\PYG{p}{;}
                                                      \PYG{c+c1}{// Polarität des Ausgangs:}
                                                      \PYG{c+c1}{// Ist der Channel \PYGZdq{}activ\PYGZdq{}}
                                                      \PYG{c+c1}{// so wird der Ausgang auf}
                                                      \PYG{c+c1}{//  \PYGZdq{}high\PYGZdq{} gesetzt (LED}
                                                      \PYG{c+c1}{// leuchtet)}

    \PYG{c+c1}{// Wir haben die OCInit Struktur nun so befüllt, dass das gewünschte PWM}
    \PYG{c+c1}{// Signal an einem internen Timer Channel erzeugt wird}
    \PYG{c+c1}{// Der Timer 2 besitzt jedoch mehrere interne Channels, die wir mit dieser}
    \PYG{c+c1}{// Struktur konfigurieren können}
    \PYG{c+c1}{// Wir wissen aber bereits, dass Pin A.5 nur mit dem Channel 1 des Timers 2}
    \PYG{c+c1}{// verbunden werden kann}
    \PYG{c+c1}{// Daher müssen wir den Channel 1 von Timer 2 mit unserer Struktur}
    \PYG{c+c1}{// konfigurieren}
    \PYG{c+c1}{// Dazu verwenden wir die Funktion \PYGZdq{}TIM\PYGZus{}OC1Init\PYGZdq{} (der 1er steht hier für}
    \PYG{c+c1}{// den gewünschten Channel 1)}
    \PYG{n}{TIM\PYGZus{}OC1Init}\PYG{p}{(}\PYG{n}{TIM2}\PYG{p}{,} \PYG{o}{\PYGZam{}}\PYG{n}{TIM\PYGZus{}OCInitStructure}\PYG{p}{);}

    \PYG{c+c1}{// Jetzt ist die Timer Basis und der internen Channel 1 von Timer 2 so}
    \PYG{c+c1}{// konfiguriert, dass der Timer ein PWM Signal am internen Channel 1 ausgibt}
    \PYG{c+c1}{// Die Periodendauer ist 1s und die Pulsweite 0.5s}
    \PYG{c+c1}{// Außerdem haben wir den internen Channel 1 des Timers mit dem Ausgang}
    \PYG{c+c1}{// von Pins A.5 verbunden}
    \PYG{c+c1}{// Die Konfiguration ist abgeschlossen und es fehlt nur mehr ein letzter}
    \PYG{c+c1}{// Schritt:}
    \PYG{c+c1}{// Wir müssen den Timer starten und die LED beginnt zu blinken :\PYGZhy{})}
    \PYG{c+c1}{// Wer ganz genau aufgepasst hat, wird auch bemerkt haben, dass die LED}
    \PYG{c+c1}{// hier doppelt so schnell blinkt (sie toggelt ja alle 0.5s)}
    \PYG{n}{TIM\PYGZus{}Cmd}\PYG{p}{(}\PYG{n}{TIM2}\PYG{p}{,} \PYG{n}{ENABLE}\PYG{p}{);}

    \PYG{c+c1}{// Endlosschleife}
    \PYG{k}{while} \PYG{p}{(}\PYG{l+m+mi}{1}\PYG{p}{);}

    \PYG{c+c1}{// Im Gegensatz zu Beispiel 2A ist die Endlosschleife dieses Mal aber}
    \PYG{c+c1}{// eigentlich gar nicht nötig}
    \PYG{c+c1}{// Im Beispiel 2A musste die CPU aktiv bleiben, um jederzeit auf einen}
    \PYG{c+c1}{// auftretenden Interrupt reagieren zu können}
    \PYG{c+c1}{// Das Blinken wird jetzt aber durch eine Hardware\PYGZhy{}PWM OHNE Zuhilfenahme}
    \PYG{c+c1}{// des Kerns bewerkstelligt (es gibt ja auch keine Interrupts mehr)}
    \PYG{c+c1}{// Besonders deutlich wird dieser Umstand, wenn man im Debug\PYGZhy{}Modus nach}
    \PYG{c+c1}{// der Initialisierung die Programmabarbeitung unterbricht (bzw. beendet):}
    \PYG{c+c1}{// Die LED blinkt dann immer noch :\PYGZhy{})}
    \PYG{c+c1}{// Alternativ könnten wir auch die \PYGZdq{}while(1)\PYGZdq{} Schleife entfernen, dann}
    \PYG{c+c1}{// beendet der Controller seine Arbeit nach der Konfiguration, die LED}
    \PYG{c+c1}{// blinkt aber trotzdem :\PYGZhy{})}

\PYG{p}{\PYGZcb{}}
\end{Verbatim}
